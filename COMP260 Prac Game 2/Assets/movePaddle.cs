﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class movePaddle : MonoBehaviour {
    private Rigidbody rigidbody;
	public Vector3 movement;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		float inputX = Input.GetAxis ("Horizontal");
		float inputZ = Input.GetAxis ("Vertical");

		movement = new Vector3 (
			speed * inputX,0,
			speed * inputZ);
	}
    public float speed = 20f;
	public float maxSpeed = 100f;

    public float force = 10f;

    void FixedUpdate()
    {
       /* Vector3 pos;
        pos.x = Input.GetAxis("Horizontal");
        pos.z = Input.GetAxis("Vertical");
        pos.y = 0;
		Vector2 velocity = pos * speed;
		rigidbody.position = velocity;
		*/
		/*Vector3 direction1;
		direction1.x = Input.GetAxis("Horizontal");
		direction1.z = Input.GetAxis("Vertical");
		direction1.y = 0;
		// scale by the maxSpeed parameter
		Vector3 velocity1 = direction1 * maxSpeed;

		// move the object
		rigidbody.position = velocity1 * Time.deltaTime;
		rigidbody.velocity = velocity1;

		*/
		rigidbody.velocity = movement;

       
    }



    private Vector3 GetMousePosition()
    {
        // create a ray from the camera 
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }

}
