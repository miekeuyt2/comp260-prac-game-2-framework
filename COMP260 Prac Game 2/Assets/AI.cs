﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {
    GameObject puckk;
    public GameObject target;
    Rigidbody rigidbody;
    Vector3 movement;


    float speed = 20f;
    Vector2 direction;
    Vector2 velocity;
    // Use this for initialization
    void Start () {
        target = GameObject.Find("Puck");
        rigidbody = GetComponent<Rigidbody>();
       
    }
	
	// Update is called once per frame
	void Update () {
        /*float inputX = puckk.transform.position.x;
        float inputZ = puckk.transform.position.z;
      
        movement = new Vector3(
            speed * inputX, 0,
            speed * inputZ);*/
        Vector3 direction;

        if (target.transform.position.x < transform.position.x)
        {
            direction.x = transform.position.x - target.transform.position.x;
        }
        else
        {

            direction.x = target.transform.position.x - transform.position.x;
        }

        if (target.transform.position.z < transform.position.z)
        {
            direction.z = transform.position.z - target.transform.position.z;
        }
        else
        {
            direction.z = target.transform.position.z - transform.position.z;
        }
        if (target.transform.position.y < transform.position.y)
        {
            direction.y = transform.position.y - target.transform.position.y;
        }
        else
        {
            direction.y = target.transform.position.y - transform.position.y;
        }
        direction = direction.normalized;

         velocity = direction * speed;
        //transform.Translate(velocity * Time.deltaTime);




    }

    private void FixedUpdate()
    {
        rigidbody.velocity = velocity;// * Time.deltaTime;

    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
       //Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
       Vector2 direction = target.transform.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

}
